--File ripple_carry_adder_11bit.vhd
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ripple_carry_adder_11bit is
  port (Cin      :        std_logic;
        A, B     : in     signed (10 downto 0);
        S        : out    signed (10 downto 0);
        Co       : buffer std_logic;
        Overflow : out    std_logic);
end ripple_carry_adder_11bit;

architecture Behavior of ripple_carry_adder_11bit is
  signal COUT : std_logic_vector(9 downto 0);

  component full_adder
    port (Ci   :     std_logic;
          x, y : in  std_logic;
          s    : out std_logic;
          Co   : out std_logic);
  end component;

begin


  fa0 : full_adder port map
    (x => A(0), y => B(0), Ci => Cin, s => S(0), Co => COUT(0));

  fa1 : full_adder port map
    (x => A(1), y => B(1), Ci => COUT(0), s => S(1), Co => COUT(1));

  fa2 : full_adder port map
    (x => A(2), y => B(2), Ci => COUT(1), s => S(2), Co => COUT(2));

  fa3 : full_adder port map
    (x => A(3), y => B(3), Ci => COUT(2), s => S(3), Co => COUT(3));

  fa4 : full_adder port map
    (x => A(4), y => B(4), Ci => COUT(3), s => S(4), Co => COUT(4));

  fa5 : full_adder port map
    (x => A(5), y => B(5), Ci => COUT(4), s => S(5), Co => COUT(5));

  fa6 : full_adder port map
    (x => A(6), y => B(6), Ci => COUT(5), s => S(6), Co => COUT(6));

  fa7 : full_adder port map
    (x => A(7), y => B(7), Ci => COUT(6), s => S(7), Co => COUT(7));

  fa8 : full_adder port map
    (x => A(8), y => B(8), Ci => COUT(7), s => S(8), Co => COUT(8));

  fa9 : full_adder port map
    (x => A(9), y => B(9), Ci => COUT(8), s => S(9), Co => COUT(9));

  fa10 : full_adder port map
    (x => A(10), y => B(10), Ci => COUT(9), s => S(10), Co => Co);

  Overflow <= Co xor COUT(9);


end Behavior;




