library ieee;
USE ieee.std_logic_1164.all;

ENTITY Tff_with_and_port IS
PORT (T, CLK, Clear: IN STD_LOGIC;
     Q: BUFFER STD_LOGIC;
	  EN_OUT: OUT STD_LOGIC);
END Tff_with_and_port;

ARCHITECTURE structure of Tff_with_and_port IS

COMPONENT T_flipflop IS
PORT(T, CLK, Clear: IN STD_LOGIC;
     Q: OUT STD_LOGIC);
END COMPONENT;

BEGIN
tff: T_flipflop PORT MAP
(T,CLK,Clear,Q);

EN_OUT<=Q AND T;

end structure;