--File CONTROL_UNIT.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity CONTROL_UNIT is
  port (START                        : in     std_logic;
        CNT_EN                       : buffer std_logic;
        DONE                         : buffer std_logic;
        RESET                        : in     std_logic;
        CNT_RESET                    : buffer std_logic;
        ADDRESS                      : in     unsigned(9 downto 0);
        C11_EN                       : buffer std_logic;
        C11_RST                      : buffer std_logic;
        L1, L2, L3                   : buffer std_logic;
        CLOCK                        : in     std_logic;
        SEL                          : buffer std_logic_vector(1 downto 0);
        SEL2                         : buffer std_logic;
        CSA, CSB, WRA, WRB, RDA, RDB : buffer std_logic;  --PER RST DELLA SOMMA
        RST_R8                       : buffer std_logic);
end CONTROL_UNIT;

architecture Behavior of CONTROL_UNIT is

  type State_type is (IDLE, LOAD_M, READ_A, LOAD_R, S1, S2, S3, S4, P_E, INCR, D);
  signal y_Q, Y_D : State_type;  -- y_Q is present state, y_D is next state

begin

  State_transition : process (START, ADDRESS, y_Q, Y_D)  -- state table 
  begin
    case y_Q is

      when IDLE =>
        if(START = '1') then Y_D <= LOAD_M;
        else Y_D                 <= IDLE;
        end if;

      when LOAD_M =>
        if (ADDRESS = "1111111111") then Y_D <= READ_A;
        else Y_D                             <= LOAD_M;
        end if;

      when READ_A => Y_D <= LOAD_R;

      when LOAD_R => Y_D <= S1;

      when S1 => Y_D <= S2;

      when S2 => Y_D <= S3;

      when S3 => Y_D <= S4;

      when S4 => Y_D <= P_E;

      when P_E => Y_D <= INCR;

      when INCR =>
        if ((ADDRESS) = "1111111111") then Y_D <= D;
        else Y_D                               <= READ_A;
        end if;


      when D =>
        if (START = '0') then Y_D <= IDLE;
        else Y_D                  <= D;
        end if;

    end case;
  end process State_transition;

  Register_process : process (CLOCK, RESET)  -- state flip-flops 
  begin
    if RESET = '0' then
      Y_Q <= IDLE;
    elsif(rising_edge(CLOCK)) then
      Y_Q <= y_D;
    end if;
  end process Register_process;

  Output_decode : process(CNT_EN, DONE, CNT_RESET, C11_EN, C11_RST, L1, L2, L3, SEL, SEL2, CSA, CSB, WRA, WRB, RDA, RDB, RST_R8, Y_Q)
  --Output_decode: PROCESS(Y_Q)
  begin

    case Y_Q is

      when IDLE =>
        L1        <= '0'; L2 <= '0'; L3 <= '0'; SEL2 <= '0'; RST_R8 <= '0'; CSA <= '0'; WRB <= '1'; RDB <= '0';
        CNT_RESET <= '0'; C11_RST <= '0'; CNT_EN <= '0'; DONE <= '0'; CSB <= '0';

      when LOAD_M =>
        CNT_RESET <= '1'; CSA <= '1'; WRA <= '1'; RDA <= '0'; CNT_EN <= '1';

      when READ_A =>
        WRA <= '0'; RDA <= '1'; CNT_EN <= '0';

      when LOAD_R =>
        L1  <= '1'; L2 <= '1'; L3 <= '1'; RST_R8 <= '1'; SEL2 <= '0';
        SEL <= "00"; SEL2 <= '0';

      when S1 =>
        L1 <= '0'; L2 <= '0'; L3 <= '0'; SEL2 <= '1';

      when S2 =>
        SEL <= "01";

      when S3 =>
        SEL <= "10";

      when S4 =>
        SEL <= "11"; RDB <= '0';

      when P_E =>
        C11_EN <= '1'; C11_RST <= '1'; CSB <= '1';

      when INCR =>
        CNT_EN <= '1'; C11_EN <= '0'; CSB <= '0';

      when D =>
        DONE <= '1';

    end case;

  end process Output_decode;

end BEHAVIOR;
