
State Machine - |NEURAL_NETWORK|CONTROL_UNIT:CU|y_Q
Name y_Q.D y_Q.INCR y_Q.P_E y_Q.S4 y_Q.S3 y_Q.S2 y_Q.S1 y_Q.LOAD_R y_Q.READ_A y_Q.LOAD_M y_Q.IDLE 
y_Q.IDLE 0 0 0 0 0 0 0 0 0 0 0 
y_Q.LOAD_M 0 0 0 0 0 0 0 0 0 1 1 
y_Q.READ_A 0 0 0 0 0 0 0 0 1 0 1 
y_Q.LOAD_R 0 0 0 0 0 0 0 1 0 0 1 
y_Q.S1 0 0 0 0 0 0 1 0 0 0 1 
y_Q.S2 0 0 0 0 0 1 0 0 0 0 1 
y_Q.S3 0 0 0 0 1 0 0 0 0 0 1 
y_Q.S4 0 0 0 1 0 0 0 0 0 0 1 
y_Q.P_E 0 0 1 0 0 0 0 0 0 0 1 
y_Q.INCR 0 1 0 0 0 0 0 0 0 0 1 
y_Q.D 1 0 0 0 0 0 0 0 0 0 1 
