--File counter_10bit.vhd
library ieee;
use ieee.std_logic_1164.all;

entity counter_10bit is
  port (CLK, EN, RESET : in  std_logic;
        COUT           : out std_logic_vector(0 to 9);
        Q              : out std_logic);
end counter_10bit;

architecture Behavior of counter_10bit is
  signal T_INT : std_logic_vector(0 to 8);

  component Tff_with_and_port is
    port (T, CLK, Clear : in     std_logic;
          Q             : buffer std_logic;
          EN_OUT        : out    std_logic);
  end component;

  component T_flipflop is
    port(T, CLK, Clear : in  std_logic;
         Q             : out std_logic); end component;

begin tff0 : Tff_with_and_port port map (EN, CLK, RESET, COUT(0), T_INT(0));

      gen_tff : for i in 1 to 8 generate
        tffi : Tff_with_and_port port map (T_INT(i-1), CLK, RESET, COUT(i), T_INT(i));
      end generate;

      tff25 : T_flipflop port map (T_INT(8), CLK, RESET, COUT(9));

end Behavior;
