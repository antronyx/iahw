--File counter_11bit.vhd
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter_11bit is
  port (CLK, EN, RESET : in  std_logic;
        COUT           : out unsigned(0 to 10);
        Q              : out std_logic);
end counter_11bit;

architecture Behavior of counter_11bit is
  signal T_INT : std_logic_vector(0 to 9);

  component Tff_with_and_port is
    port (T, CLK, Clear : in     std_logic;
          Q             : buffer std_logic;
          EN_OUT        : out    std_logic);
  end component;

  component T_flipflop is
    port(T, CLK, Clear : in  std_logic;
         Q             : out std_logic);
  end component;

begin tff0 : Tff_with_and_port port map (EN, CLK, RESET, COUT(0), T_INT(0));

      gen_tff : for i in 1 to 9 generate
        tffi : Tff_with_and_port port map (T_INT(i-1), CLK, RESET, COUT(i), T_INT(i));
      end generate;

      tff25 : T_flipflop port map (T_INT(9), CLK, RESET, COUT(10));

end Behavior;
