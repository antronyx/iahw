--FILE MEMORY.VHD

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity memory is
  port(CLK      : in  std_logic;
       DATA_IN  : in  signed(7 downto 0);
       ADDRESS  : in  unsigned(9 downto 0);  --pointer for the word cell
       CS       : in  std_logic;
       WR       : in  std_logic;
       RD       : in  std_logic;
       DATA_OUT : out signed(7 downto 0));
end memory;


architecture Be of memory is
  subtype word is signed(7 downto 0);
  type mem is array(0 to 1023) of word;
  signal MODE_BUS : std_logic_vector(2 downto 0);  --CS,WR,RD
  signal memory   : mem := (others => "00000000");
begin
  MODE_BUS <= CS & WR & RD;
  main_p : process(CLK) is
  begin
    if rising_edge(CLK) then
      if MODE_BUS = "101" then
        DATA_OUT <= memory(to_integer(ADDRESS));

      elsif
        MODE_BUS = "110" then
        memory(to_integer(ADDRESS)) <= DATA_IN;
        DATA_OUT <= "00000000";--needed avoiding problems of indetermination
      else
        DATA_OUT <= "00000000";--if "bus_code" is wrong return no data
      end if;
    end if;

  end process main_p;

end be;
