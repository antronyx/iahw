--File reg8.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reg8 is
  generic (N : integer := 8);
  port (R     : in  signed(N-1 downto 0);
        RESET : in  std_logic;
        LOAD  : in  std_logic;
        Clock : in  std_logic;
        Q     : out signed(N-1 downto 0));
end reg8;

architecture Behavior of reg8 is
begin
  process (Clock)
  begin

    if RESET = '0' then
      Q <="00000000";
    elsif (Clock'event and Clock = '1') then
      if LOAD = '1' then
        Q <= R;
      end if;
    end if;
  end process;
end Behavior;
