--File mux2to1_8bit.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux2to1_8bit is
  port(U, V : in  signed (7 downto 0);
       S    : in  std_logic;
       M    : out signed(7 downto 0));
end mux2to1_8bit;

architecture structure of mux2to1_8bit is

begin
  with S select
    M <= U when '0',
    V when others;


end structure;
