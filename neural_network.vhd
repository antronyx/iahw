--File NEURAL_NETWORK.vhd
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity NEURAL_NETWORK is
  port(DATA_IN    : in  signed(7 downto 0);
       CLOCK      : in  std_logic;
       START      : in  std_logic;
       DONE       : out std_logic;
       RESET      : in  std_logic;
       POS_VALUES : out unsigned(10 downto 0)
       );
end NEURAL_NETWORK;

architecture STRUCTURE of NEURAL_NETWORK is


  component DATA_PATH
    port(D_IN       : in  signed (7 downto 0);
         CNT_RESET  : in  std_logic;
         C11_RST    : in  std_logic;
         CNT_EN     : in  std_logic;
         C11_EN     : in  std_logic;
         L1, L2, L3 : in  std_logic;
         CLOCK      : in  std_logic;
         SEL        : in  std_logic_vector(1 downto 0);
         SEL2       : in  std_logic;
         POS_VALUES : out unsigned(10 downto 0);
         DATA_B     : out signed(7 downto 0);
         ADDRESS    : out std_logic_vector(0 to 9);
         RST_R8     : in  std_logic
         );
  end component;

  component CONTROL_UNIT
    port (START                        : in     std_logic;
          CNT_EN                       : buffer std_logic;
          DONE                         : buffer std_logic;
          RESET                        : in     std_logic;
          CNT_RESET                    : buffer std_logic;
          ADDRESS                      : in     unsigned(9 downto 0);
          C11_EN                       : buffer std_logic;
          C11_RST                      : buffer std_logic;
          L1, L2, L3                   : buffer std_logic;
          CLOCK                        : in     std_logic;
          SEL                          : buffer std_logic_vector(1 downto 0);
          SEL2                         : buffer std_logic;
          CSA, CSB, WRA, WRB, RDA, RDB : buffer std_logic;  --PER RST DELLA SOMMA
          RST_R8                       : buffer std_logic);
  end component;

  component memory
    port(CLK        : in  std_logic;
         DATA_IN    : in  signed(7 downto 0);
         ADDRESS    : in  unsigned(9 downto 0);  --pointer for the cell
         CS, WR, RD : in  std_logic;
         DATA_OUT   : out signed(7 downto 0)
         );
  end component;

  signal C11_RST                      : std_logic;
  signal CNT_EN                       : std_logic;
  signal L1, L2, L3                   : std_logic;
  signal DATA_INB                     : signed(7 downto 0);
  signal D_OUTA                       : signed(7 downto 0);
  signal RST_R8                       : std_logic;
  signal CNT_RESET                    : std_logic;
  signal ADDRESS                      : unsigned(9 downto 0);
  signal ADDRESS_DP                   : std_logic_vector(0 to 9);
  signal C11_EN                       : std_logic;
  signal SEL                          : std_logic_vector(1 downto 0);
  signal SEL2                         : std_logic;
  signal CSA, CSB, WRA, WRB, RDA, RDB : std_logic;
  signal D_OUTB                       : signed(7 downto 0);
  signal Pos_signal_inv: unsigned (0 to 10);
begin
  memoryA : memory port map
    (CLOCK, DATA_IN, ADDRESS, CSA, WRA, RDA, D_OUTA);

  memoryB : memory port map
    (CLOCK, DATA_INB, ADDRESS, CSB, WRB, RDB, D_OUTB);

  CU : CONTROL_UNIT port map
    (START, CNT_EN, DONE, RESET, CNT_RESET, ADDRESS, C11_EN, C11_RST, L1, L2, L3, CLOCK, SEL, SEL2, CSA, CSB, WRA, WRB, RDA, RDB, RST_R8);

  DP : DATA_PATH port map
    (D_OUTA, CNT_RESET, C11_RST, CNT_EN, C11_EN, L1, L2, L3, CLOCK, SEL, SEL2, POS_signal_inv, DATA_INB, ADDRESS_DP, RST_R8);

  gen_add : for i in 9 downto 0 generate
    ADDRESS(i) <= ADDRESS_DP(i);
  end generate;


  reverse:for i in 10 downto 0 generate
    POS_VALUES(i)<=Pos_signal_inv(i);
  end generate;


end STRUCTURE;
