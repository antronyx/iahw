--File T_flipflop.vhd
library ieee;
use ieee.std_logic_1164.all;

entity T_flipflop is
  port(T, CLK, Clear : in     std_logic;
       Q             : buffer std_logic);
end T_flipflop;

architecture Behavior of T_flipflop is

  signal D : std_logic;

  component flipflop is
    port (D, Clock, Resetn : in  std_logic;
          Q                : out std_logic);
  end component;

begin

  D <=(Q and (not T))or(T and (not Q));

  ff : flipflop port map
    (D, CLK, Clear, Q);
end Behavior;
