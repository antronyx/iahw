--File counter8bit.vhd
--Created for testbench

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity counter8bit is
  port(NUM : out signed(7 downto 0) := (others => '0');
       CLK : in  std_logic);

end counter8bit;

architecture be of counter8bit is
signal NUM_s : signed(7 downto 0) := (others => '0');
begin

  aa : process(clk) is
  begin

        
      if rising_edge(clk) then
      num_s <= num_s + x"1";
      end if;
      

  end process aa;
num<=num_s;

end be;
