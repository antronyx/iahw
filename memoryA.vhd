library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity memoryA is
  port(CLK: in std_logic;
   DATA_IN:in signed(7 downto 0);
   ADDRESS:in unsigned(9 downto 0); --pointer for the cell
   MODE_BUS:in std_logic_vector(2 downto 0); --CS,WR,RD
   DATA_OUT:out signed(7 downto 0)
       );
end memoryA;


architecture Be of memoryA is
  subtype word is signed(7 downto 0);
  type mem is array(0 to 1023) of word;

  signal memory:mem:=(others=>"00000000");
  signal pointer:unsigned(9 downto 0);
begin
  pointer<=ADDRESS;
  main_p:process(CLK) is
  begin
  if rising_edge(CLK) then
    if MODE_BUS="101" then
      DATA_OUT<=memory(to_integer(ADDRESS));
    end if;

    if MODE_BUS="110" then
      memory(to_integer(ADDRESS))<=DATA_IN;
    end if;
  end if;
      
  end process main_p;
  
end be;
