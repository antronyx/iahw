--File DATA_PATH.vhd
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DATA_PATH is
  port(D_IN       : in  signed (7 downto 0);
       CNT_RESET  : in  std_logic;
       C11_RST    : in  std_logic;
       CNT_EN     : in  std_logic;
       C11_EN     : in  std_logic;
       L1, L2, L3 : in  std_logic;
       CLOCK      : in  std_logic;
       SEL        : in  std_logic_vector(1 downto 0);
       SEL2       : in  std_logic;
       POS_VALUES : out unsigned(10 downto 0);
       DATA_B     : out signed(7 downto 0);
       ADDRESS    : out std_logic_vector(0 to 9);
       RST_R8     : in  std_logic
       );
end DATA_PATH;

architecture BEHAVIOR of DATA_PATH is

  signal R1_OUT, R2_OUT, R3_OUT : signed (7 downto 0);
  signal MUX1_OUT               : signed(10 downto 0);
  signal R_SUM                  : signed(10 downto 0);
  signal ADD_OUT                : signed(10 downto 0);
  signal SUM                    : signed(10 downto 0);
  signal OVF                    : std_logic_vector(1 downto 0);
  signal C11_E                  : std_logic;
  signal V1, V2, V3, V4         : signed(10 downto 0);

  component reg8
    generic (N : integer := 8);
    port (R     : in  signed(N-1 downto 0);
          RESET : in  std_logic;
          LOAD  : in  std_logic;
          Clock : in  std_logic;
          Q     : out signed(N-1 downto 0));
  end component;

  component mux_11bit_4to1 is
    port(U, V, W, X : in  signed(10 downto 0);
         S          : in  std_logic_vector(1 downto 0);
         M          : out signed(10 downto 0));
  end component;

  component counter_11bit is
    port (CLK, EN, RESET : in  std_logic;
          COUT           : out unsigned(0 to 10);
          Q              : out std_logic
          );
  end component;

  component reg11 is
    generic (N : integer := 11);
    port (R     : in  signed(N-1 downto 0);
          Clock : in  std_logic;
          Q     : out signed(N-1 downto 0));
  end component;

  component mux2to1_11bit is
    port(U, V : in  signed (10 downto 0);
         S    : in  std_logic;
         M    : out signed(10 downto 0));
  end component;

  component mux3to1_8bit is
    port(U, V, W : in  signed (7 downto 0);
         S       : in  std_logic_vector(1 downto 0);
         M       : out signed(7 downto 0));
  end component;

  component counter_10bit is
    port (CLK, EN, RESET : in  std_logic;
          COUT           : out std_logic_vector(0 to 9);
          Q              : out std_logic
          );
  end component;

  component mux2to1_8bit is
    port(U, V : in  signed (7 downto 0);
         S    : in  std_logic;
         M    : out signed(7 downto 0));
  end component;

  component ripple_carry_adder_11bit is
    port (Cin      :        std_logic;
          A, B     : in     signed (10 downto 0);
          S        : out    signed (10 downto 0);
          Co       : buffer std_logic;
          Overflow : out    std_logic);
  end component;

begin

  REG1 : reg8 port map
    (D_IN, RST_R8, L1, CLOCK, R1_OUT);

  REG2 : reg8 port map
    (R1_OUT, RST_R8, L2, CLOCK, R2_OUT);

  REG3 : reg8 port map
    (R2_OUT, RST_R8, L3, CLOCK, R3_OUT);

  V1(10)         <= R1_OUT(7);
  V1(9)          <= R1_OUT(7);
  V1(8)          <= R1_OUT(7);
  V1(7)          <= R1_OUT(7);
  V1(6 downto 0) <= R1_OUT(7 downto 1);
  V2(10)         <= R2_OUT(7);
  V2(9)          <= R2_OUT(7);
  V2(8)          <= R2_OUT(7);
  V2(7 downto 2) <= R2_OUT(5 downto 0);
  V2(1 downto 0) <= "00";
  V3(10)         <= not R2_OUT(7);
  V3(9)          <= not R2_OUT(7);
  V3(8)          <= not R2_OUT(7);
  V3(7)          <= not R2_OUT(7);
  V3(6)          <= not R2_OUT(7);
  V3(5 downto 0) <= not(R2_OUT(7 downto 2));
  V4(10)         <= not (R3_OUT(7));
  V4(9)          <= not(R3_OUT(7));
  V4(8)          <= not (R3_OUT(7));
  V4(7)          <= not (R3_OUT(7));
  V4(6 downto 0) <= not (R3_OUT(6 downto 0));

  mux4to1 : mux_11bit_4to1 port map
    (V1, V2, V3, V4, SEL, MUX1_OUT);

  counter : counter_10bit port map
    (CLOCK, CNT_EN, CNT_RESET, ADDRESS);

  add1 : ripple_carry_adder_11bit port map
    (SEL(1), MUX1_OUT, SUM, ADD_OUT);

  mux2to1_1 : mux2to1_11bit port map
    ("00000000000", ADD_OUT, SEL2, R_SUM);

  register_sum : reg11 port map
    (R_SUM, CLOCK, SUM);

  C11_E <= C11_EN and (not SUM(10));
  c11 : counter_11bit port map
    (CLOCK, C11_E, C11_RST, POS_VALUES);

  OVF(1) <= (SUM(9)or SUM(8)or SUM(7))xor SUM(10);
  OVF(0) <= (SUM(9)and SUM(8)and SUM(7)) xor SUM(10);

  mux3to1 : mux3to1_8bit port map
    (SUM(7 downto 0), "10000000", "01111111", OVF, DATA_B);

end BEHAVIOR;
