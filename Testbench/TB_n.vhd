library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_n is
end tb_n;


architecture tb of tb_n is
  component NEURAL_NETWORK is
    port(DATA_IN    : in  signed(7 downto 0);
         CLOCK      : in  std_logic;
         START      : in  std_logic;
         DONE       : out std_logic;
         RESET      : in  std_logic;
         POS_VALUES : out unsigned(10 downto 0)
         );
  end component;



  signal data_in_s    : signed(7 downto 0)           := (others => '0');
  signal clk_s        : std_logic                    := '0';
  signal start_s      : std_logic;
  signal DONE_s       : std_logic;
  signal reset_s      : std_logic;
  signal pos_values_s : unsigned (10 downto 0);
begin



  nn : neural_network port map(data_in_s, clk_s, start_s, done_s, reset_s, pos_values_s);

  clock_signal : process is
  begin
    clk_s <= '0';
    wait for 1 ns;
    clk_s <= '1';
    wait for 1 ns;
  end process clock_signal;

  start_signal : process is
  begin

    start_s<='0';
    wait for 5 ns;
    
    start_s <= '1';
    wait for 100000000 ns;
    --wait for 5 ns;
    --start_s <= '0';
    --wait for 100000 ns;
  end process start_signal;

  reset_signal : process is
  begin
    reset_s <= '0';
    wait for 2 ns;
    reset_s <= '1';
    wait for 1000020 ns;
  end process reset_signal;



end tb;
