--File mux4to1
library ieee;
use ieee.std_logic_1164.all;

entity mux4to1 is
  port(U, V, W, X : in  std_logic;
       S          : in  std_logic_vector(1 downto 0);
       M          : out std_logic);
end mux4to1;

architecture structure of mux4to1 is

begin
  with S select
    M <= U when "00",
    V when "01",
    W when "10",
    X when others;
end structure;
