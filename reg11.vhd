--File reg11.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity reg11 is
  generic (N : integer := 11);
  port (R     : in  signed(N-1 downto 0);
        Clock : in  std_logic;
        Q     : out signed(N-1 downto 0));
end reg11;

architecture Behavior of reg11 is
begin
  process (Clock)
  begin
    if (Clock'event and Clock = '1') then
      Q <= R; end if;
  end process;
end Behavior;
