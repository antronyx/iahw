--File  mux2to1_11bit
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux2to1_11bit is
  port(U, V : in  signed (10 downto 0);
       S    : in  std_logic;
       M    : out signed(10 downto 0));
end mux2to1_11bit;

architecture structure of mux2to1_11bit is

begin
  with S select
    M <=U when '0',
    V when others;
end structure;
