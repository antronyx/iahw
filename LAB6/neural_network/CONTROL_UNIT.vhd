library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

ENTITY CONTROL_UNIT IS
PORT (START:IN STD_LOGIC;
      CNT_EN: BUFFER STD_LOGIC; 
      DONE: BUFFER STD_LOGIC;
		RESET: IN STD_LOGIC;
      CNT_RESET: BUFFER STD_LOGIC;
	   ADDRESS:IN UNSIGNED(9 DOWNTO 0);
		C11_EN: BUFFER STD_LOGIC;
	   C11_RST: BUFFER STD_LOGIC;
      L1,L2,L3: BUFFER STD_LOGIC;
      CLOCK: IN STD_LOGIC;
      SEL: BUFFER STD_LOGIC_VECTOR(1 DOWNTO 0); 
	   SEL2: BUFFER STD_LOGIC;
	   CSA,CSB,WRA,WRB,RDA,RDB: BUFFER STD_LOGIC;  --PER RST DELLA SOMMA
		RST_R8: BUFFER STD_LOGIC);
END CONTROL_UNIT;

ARCHITECTURE Behavior of CONTROL_UNIT IS

TYPE State_type IS (IDLE,LOAD_M,READ_A,LOAD_R,S1,S2,S3,S4,P_E,INCR,D);
SIGNAL y_Q, Y_D : State_type;  -- y_Q is present state, y_D is next state

BEGIN 

State_transition: PROCESS (START,ADDRESS,y_Q,Y_D) -- state table 
BEGIN 
  CASE y_Q IS 
  
WHEN IDLE=> IF(START = '1')  THEN Y_D <= LOAD_M; 
ELSE Y_D <= IDLE;  
 END IF; 

 WHEN LOAD_M=> IF (ADDRESS = "1111111111")  THEN Y_D <= READ_A; 
ELSE Y_D <= LOAD_M;  
 END IF; 
 
 WHEN READ_A=> Y_D <= LOAD_R; 

 WHEN LOAD_R=> Y_D <= S1; 
 
 WHEN S1=> Y_D <= S2; 
 
 WHEN S2=> Y_D <= S3;
 
 WHEN S3=> Y_D <= S4; 

 WHEN S4=> Y_D <= P_E; 

 WHEN P_E=> Y_D <= INCR; 

 WHEN INCR=> IF ((ADDRESS) = "1111111111")  THEN Y_D <= D; 
ELSE Y_D <= READ_A;  
 END IF; 
  

 WHEN D=> IF (START = '0')  THEN Y_D <= IDLE; 
ELSE Y_D <= D;  
 END IF;
 
END CASE;
 END PROCESS State_transition;
 
 Register_process: PROCESS (CLOCK,RESET) -- state flip-flops 
BEGIN 
IF RESET = '0' THEN     
 Y_Q <= IDLE;       
 ELSIF( rising_edge(CLOCK) ) THEN  
 Y_Q <= y_D;       
 END IF; 
END PROCESS Register_process;

Output_decode: PROCESS(CNT_EN,DONE,CNT_RESET,C11_EN,C11_RST, L1,L2,L3,SEL,SEL2,CSA,CSB,WRA,WRB,RDA,RDB,RST_R8,Y_Q)
  --Output_decode: PROCESS(Y_Q)
BEGIN

CASE Y_Q IS

WHEN IDLE=> 
L1<= '0'; L2<= '0'; L3<= '0'; SEL2<= '0';  RST_R8<='0';  CSA<= '0';  WRB<= '1'; RDB<= '0';
 CNT_RESET<= '0'; C11_RST<= '0'; CNT_EN<= '0'; DONE<='0'; CSB<= '0';

WHEN LOAD_M=> 
CNT_RESET<= '1';  CSA<= '1'; WRA<= '1'; RDA<= '0'; CNT_EN<= '1';

WHEN READ_A=>
WRA<= '0'; RDA<= '1'; CNT_EN<= '0'; 

WHEN LOAD_R=> 
L1<= '1'; L2<= '1'; L3<= '1'; RST_R8<='1'; SEL2<= '0'; 
SEL<= "00"; SEL2<='0'; 

WHEN S1=> 
L1<= '0'; L2<= '0'; L3<= '0'; SEL2<= '1';

WHEN S2=> 
SEL<= "01";

WHEN S3=> 
SEL<= "10";

WHEN S4=> 
SEL<= "11"; RDB<= '0';

WHEN P_E=> 
C11_EN<= '1'; C11_RST<= '1'; CSB<= '1'; 

WHEN INCR=>
CNT_EN<= '1'; C11_EN<='0'; CSB<='0';

WHEN D=>
DONE<='1';

END CASE;

END PROCESS Output_decode;

END BEHAVIOR; 