--FILE TB_MEMA.VHD
library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity tb_memA is
end tb_memA;

architecture be of tb_memA is


signal CLK_s:  std_logic;
signal DATA_IN_s: signed(7 downto 0):=(others=>'0');
signal ADDRESS_s: unsigned(9 downto 0):= (others =>'0'); --pointer for the cell
signal DATA_OUT_s: signed(7 downto 0);
signal CS_s:in std_logic:=0;
signal WR_s:in std_logic:=0;
signal RD_s:in std_logic:=0;
      

component memory is
  port(CLK: in std_logic;
   DATA_IN:in signed(7 downto 0);
   ADDRESS:in unsigned(9 downto 0); --pointer for the cell
   CS:in std_logic;
   WR:in std_logic;
   RD:in std_logic;
   DATA_OUT:out signed(7 downto 0)
       );
end component;
begin
mm:memory port map(clk_s,data_in_s,address_s,CS_s,WR_s,RD_s,data_out_s);


clo:process is
begin
  clk_s<='0';
  wait for 1 ns;
  clk_s<='1';
  wait for 1 ns;
end process clo;

dt_in:process is
begin
data_in_s<=data_in_s+1;
  wait for 1 ns;
end process dt_in;

add_s:process is
begin
address_s<=address_s+1;
  wait for 1 ns;
end process add_s;




--NOW SIGNALS FOR MODE_BUS


process is
begin
---WRONG SIGNAL
  CS_S<='0';
  WR_S<='1';
  RD_S<='0';
  wait for 1024 ns;
---WRONG SIGNAL
  CS_S<='0';
  WR_S<='1';
  RD_S<='1';
  wait for 1024 ns;
  ---WRONG SIGNAL
  CS_S<='1';
  WR_S<='1';
  RD_S<='1';
  wait for 1024 ns;
  ---RIGHT SIGNAL,WRONG METHON, "INITIALIZATION" REQUIRED
  CS_S<='1';
  WR_S<='0';
  RD_S<='1';
  wait for 1024 ns;
---WRONG SIGNAL
  CS_S<='0';
  WR_S<='1';
  RD_S<='1';
  wait for 1024 ns;
  ---RIGHT SIGNAL, WRITING ON MEMORY
  CS_S<='1';
  WR_S<='1';
  RD_S<='0';
  wait for 1024 ns;
  ---RIGHT SIGNAL, READING ON MEMORY
  CS_S<='1';
  WR_S<='1';
  RD_S<='0';
  wait for 1024 ns;
---WRONG SIGNAL
  CS_S<='0';
  WR_S<='1';
  RD_S<='1';
  wait for 1024 ns;  
  
end process;



end be;





