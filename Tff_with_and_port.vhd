--File Tff_with_and_port.vhd

library ieee;
use ieee.std_logic_1164.all;

entity Tff_with_and_port is
  port (T, CLK, Clear : in     std_logic;
        Q             : buffer std_logic;
        EN_OUT        : out    std_logic);
end Tff_with_and_port;

architecture structure of Tff_with_and_port is

  component T_flipflop is
    port(T, CLK, Clear : in  std_logic;
         Q             : out std_logic);
  end component;

begin
  tff : T_flipflop port map
    (T, CLK, Clear, Q);

  EN_OUT <= Q and T;

end structure;
