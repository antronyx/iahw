--File mux3to1_8bit.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux3to1_8bit is
  port(U, V, W : in  signed (7 downto 0);
       S       : in  std_logic_vector(1 downto 0);
       M       : out signed(7 downto 0));
end mux3to1_8bit;

architecture structure of mux3to1_8bit is

begin
  with S select
    M <=U when "00",
    V when "01",
    W when others;
end structure;
