--File mux_11bit_4to1.vhd

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity mux_11bit_4to1 is
  port(U, V, W, X : in  signed(10 downto 0);
       S          : in  std_logic_vector(1 downto 0);
       M          : out signed(10 downto 0));
end mux_11bit_4to1;

architecture structure of mux_11bit_4to1 is

  component mux4to1
    port(u, v, w, x : in  std_logic;
         S          : in  std_logic_vector(1 downto 0);
         m          : out std_logic);
  end component;

begin
  gen : for i in 0 to 10 generate
    Muxi : mux4to1 port map
      (U(10-i), V(10-i), W(10-i), X(10-i), S, M(10-i));
  end generate;

end structure;
