LIBRARY ieee; 
USE ieee.std_logic_1164.all;

ENTITY full_adder IS 
PORT ( Ci: STD_LOGIC;
        x,y: IN STD_LOGIC ;
		  s: OUT STD_LOGIC;
		  Co: OUT STD_LOGIC);
END full_adder;

ARCHITECTURE Behavior OF full_adder IS
 BEGIN 
 
Co<=(x and y) or(x and Ci) or(y and Ci);
s<=x xor y xor Ci;

END Behavior;