library ieee;
USE ieee.std_logic_1164.all;

ENTITY T_flipflop IS
PORT(T, CLK, Clear: IN STD_LOGIC;
     Q: BUFFER STD_LOGIC);
END T_flipflop;

ARCHITECTURE Behavior OF T_flipflop IS

SIGNAL D :STD_LOGIC;

COMPONENT flipflop IS  
PORT (D, Clock, Resetn : IN  STD_LOGIC;   
Q   : OUT STD_LOGIC); 
END COMPONENT; 

BEGIN

D<=(Q AND (NOT T))OR(T AND (NOT Q));
 
 ff: flipflop PORT MAP
 (D,CLK,Clear,Q);
END Behavior;