LIBRARY ieee; 
USE ieee.std_logic_1164.all; 
USE ieee.numeric_std.all; 
 
ENTITY reg11 IS  
GENERIC ( N : integer:=11);  
PORT (R   : IN  SIGNED(N-1 DOWNTO 0);   
Clock: IN  STD_LOGIC;   
Q   : OUT SIGNED(N-1 DOWNTO 0)); 
END reg11; 
 
ARCHITECTURE Behavior OF reg11 IS 
BEGIN  
PROCESS (Clock)  
BEGIN  
IF (Clock'EVENT AND Clock = '1') THEN    
Q <= R;   END IF;  
END PROCESS; 
END Behavior;