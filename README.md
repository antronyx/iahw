# IAHW

IA:HW is a simple harware neural network implementation

## Installation

Just clone the repository on your workdir and compile with your favourite VHDL compiler

```bash
git clone https://gitlab.com/antronyx/iahw.git
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[GNU] GPL V 3.0